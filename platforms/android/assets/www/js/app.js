var app = angular.module('OrderApp', ['LocalStorageModule', 'ct.ui.router.extras', 'ui.bootstrap']);

// Declare app level module which depends on filters, and services
//angular.module('OrderApp', [
//  'OrderApp.filters',
//  'OrderApp.services',
//  'OrderApp.directives',
//  'OrderApp.controllers'
//]).
app.config(['$stateProvider', '$stickyStateProvider', '$urlRouterProvider', '$httpProvider',
    function ($stateProvider, $stickyStateProvider, $urlRouterProvider, $httpProvider) {
        $stickyStateProvider.enableDebug(true);
        $urlRouterProvider.otherwise("login");
        $stateProvider
            
            .state('login', {
                url: '/login',
                cache: false,
                templateUrl: '/views/login.html',
                controller: "LoginController"
            })
            .state('home', {
                url: '/home',
                abstract: true,
                templateUrl: '/views/home.html',
                controller: "HomeController"
            })            
    }
]);

app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('authInterceptorService');
});

app.run(function ($rootScope, $state, AuthService, USER_ROLES) {
 
    $rootScope.$state = $state;

    if (!AuthService.isAuthenticated())
        $state.go('login');
    //else {
    //    switch (AuthService.user().RoleId) {
    //        case USER_ROLES.DEALER: $state.go("home.dealer", {}, { reload: true });
    //            break;
    //        case USER_ROLES.SALE: $state.go("home.sale", {}, { reload: true });
    //            break;
    //        case USER_ROLES.SC: $state.go("home.sc", {}, { reload: true });
    //            break;
    //    }
    //}

    $rootScope.$on('$stateChangeStart', function (event, next, nextParams, fromState) {

        // AuthService.checkVersion(appVersion);
        if (!AuthService.isAuthenticated()) {
            if (next.name !== 'login') {
                event.preventDefault();
                $state.go('login');
            }
        }
    });
})

app.directive("select", function ($timeout, $parse) {
    return {
        restrict: 'AC',
        require: 'ngModel',
        link: function (scope, element, attrs) {
            //console.log(attrs);
            $timeout(function () {
                element.select2({ minimumResultsForSearch: "-1" });
                element.select2Initialized = true;
            });

            var refreshSelect = function () {
                if (!element.select2Initialized) return;
                $timeout(function () {
                    element.trigger('change');
                });
            };

            var recreateSelect = function () {
                if (!element.select2Initialized) return;
                $timeout(function () {
                    element.select2('destroy');
                    element.select2({ minimumResultsForSearch: "-1" });
                });
            };

            scope.$watch(attrs.ngModel, refreshSelect);

            if (attrs.ngOptions) {
                var list = attrs.ngOptions.match(/ in ([^ ]*)/)[1];
                // watch for option list change
                scope.$watch(list, recreateSelect);
            }

            if (attrs.ngDisabled) {
                scope.$watch(attrs.ngDisabled, refreshSelect);
            }
        }
    };
})
.directive("select2", function ($timeout, $parse) {
    return {
        restrict: 'AC',
        require: 'ngModel',
        link: function (scope, element, attrs) {
            //console.log(attrs);
            $timeout(function () {
                element.select2();
                element.select2Initialized = true;
            });

            var refreshSelect = function () {
                if (!element.select2Initialized) return;
                $timeout(function () {
                    element.trigger('change');
                });
            };

            var recreateSelect = function () {
                if (!element.select2Initialized) return;
                $timeout(function () {
                    element.select2('destroy');
                    element.select2();
                });
            };

            scope.$watch(attrs.ngModel, refreshSelect);

            if (attrs.ngOptions) {
                var list = attrs.ngOptions.match(/ in ([^ ]*)/)[1];
                // watch for option list change
                scope.$watch(list, recreateSelect);
            }

            if (attrs.ngDisabled) {
                scope.$watch(attrs.ngDisabled, refreshSelect);
            }
        }
    };
});
//app.run(['authService', function (authService) {
//    authService.fillAuthData();
//}]);
