﻿surveyApp.controller('TabController', function ($rootScope, $scope, $stickyState, $state, AuthService, AUTH_EVENTS, NETWORK_EVENTS, DealerService) {
    $scope.setTabVisibility = function (flag1, flag2) {
        document.getElementById("tab").style.visibility = flag1;
        document.getElementById("bar").style.visibility = flag2;
    }

    $scope.backPage = function () {
        window.history.back();
    }

    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
        if (fromState.name == "tabs.survey" && toState.name == "tabs.newsurvey") {
            $scope.setTabVisibility('hidden', 'visible');
            console.log("hidden");
        }
        else if (fromState.name == "tabs.dealers" && toState.name == "tabs.dealer-detail") {
            $scope.setTabVisibility('hidden', 'visible');
            console.log("hidden");
        }
        else if (toState.name == "tabs.survey" || toState.name == "tabs.dealers") {
            $scope.setTabVisibility('visible', 'hidden');
            console.log("hidden");
        }

        if (fromState.name == "tabs.newsurvey" && toState.name == "tabs.survey") {
            $state.reload();
            //$stickyState.reset('tabs.newsurvey');
            //console.log("Clear sticky");
        }
    });

    $rootScope.$on('$stateChangeSuccess', function () {
        $("html, body").animate({ scrollTop: 0 }, 200);
    })

    $rootScope.TIME_OUT = 60000;

    $scope.$on(AUTH_EVENTS.notAuthorized, function (event) {
        //var alertPopup = $ionicPopup.alert({
        //    title: 'Unauthorized!',
        //    template: 'You are not allowed to access this resource.'
        //});
        alert('You are not allowed to access this resource.');
    });

    $scope.$on(AUTH_EVENTS.notAuthenticated, function (event) {
        AuthService.logout();
        $state.go('login');
        //var alertPopup = $ionicPopup.alert({
        //    title: 'Session Lost!',
        //    template: 'Sorry, You have to login again.'
        //});
        alert('Sorry, You have to login again.');
    });

    $scope.$on(NETWORK_EVENTS.nointernet, function (event) {
        $ionicLoading.hide();
        //var alertPopup = $ionicPopup.alert({
        //    template: 'Không kết nối được với server'
        //});
        alert('Không kết nối được với server');
    });

    $scope.$on(NETWORK_EVENTS.timeout, function (event) {
        $ionicLoading.hide();
        //var alertPopup = $ionicPopup.alert({
        //    template: 'Kết nối timeout'
        //});
        alert('Kết nối timeout');
    });

    $scope.setCurrentUsername = function (name) {
        $scope.username = name;
    };

    //$scope.$on('$ionicView.beforeEnter', function () {
    //    console.log('enter');
    //    var stateName = $state.current.name;
    //    if (stateName === 'tabs.survey' || stateName === 'tabs.dealers' || stateName === 'tabs.account') {
    //        $rootScope.hideTabs = false;
    //    } else {
    //        $rootScope.hideTabs = true;
    //    }
    //});

    $rootScope.processRequestError = function (response) {
        if (response.status != 0 && response.status != 408) {
            //var alertPopup = $ionicPopup.alert({
            //    title: 'Thất bại!',
            //    template: err.data.message
            //});
            alert(response.data.message);
        }
    }

    $rootScope.$on('uploadImagesFinishDealer', function (event) {
        console.log('uploadImagesFinishDealer');
        $scope.$apply(function () {
            $scope.uploadImageFinish = true;
            DealerService.setUploadImageFinish(true);
        })
    });

    $scope.getUser = function() {
        $scope.user = AuthService.user();
        if (!$scope.user || !$scope.user.RoleId || $scope.user.RoleId == 1) {
            $rootScope.IsSaleRep = true;
        } else {
            $rootScope.IsSaleRep = false;
        }

        if (!$rootScope.$$phase) {
            $rootScope.$apply();
        }
    }
   
    $scope.getUser();
});